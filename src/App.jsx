import React from 'react'
import './App.css'

function App() {
  return (
    <div className="App">
      <h1>Welcome to Sign Translator!</h1>
    </div>
  )
}

export default App
